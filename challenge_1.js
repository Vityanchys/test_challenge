// #1
function Stringer() {
    this.storage = [];

    this.store = function (data) {
        if (typeof data !== 'string') throw new Error('Not a string');
        this.storage = [...this.storage, data];
    }

    this.retrieve = function () {
        return this.storage.join('');
    }
}

// #2
function Logger() {
    this.flush = function () {
        this.storage = [];
        return this.storage;
    }

    this.undo = function () {
        const lastElementNumber = this.storage.length - 1;
        return this.storage.slice(0, lastElementNumber);
    }
}

Logger.prototype = new Stringer;

// #3
function Counter() {
    this.count = function () {
        return this.storage.length;
    }

    this.size = function () {
        const joinedStorage = this.storage.join('');
        return joinedStorage.length;
    }
}

Counter.prototype = new Stringer;

// #4
const logger = new Logger();

logger.store('foo');
logger.store('Mississauga');
logger.store('this is a test');
logger.store('abcdefghijklmnop');

logger.undo();

// #5
const counter = new Counter();

counter.count();
counter.size();

counter.store('bar');
counter.store('baz');
counter.store('undone');
counter.store(null);


// #6
console.log(logger.retrieve());
console.log(counter.retrieve());