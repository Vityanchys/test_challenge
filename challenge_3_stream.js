const readingModule = (function () {
    const fs = require('fs');
    const storage = {
        wordsCounter: {},
    };

    function getSeconds(time) {
        return new Date(time).getTime() / 1000;
    }

    function sortProperties(obj) {
        let sortable = [];

        for (let item in obj) {
            sortable.push([item, obj[item]]);
        }

        const sortedValues = sortable.sort(function (a, b) {
            return a[1] - b[1];
        });

        return sortedValues.reverse().splice(0, 5);
    }

    function readFile(fileName) {
        const stream = fs.createReadStream(fileName);
        const startTime = new Date();
        let lastWordInPrevChunk = '';

        function countWords(data, counterStorage) {
            const counter = Object.assign({}, counterStorage);
            data.forEach(function (wordItem) {
                const word = wordItem.toLowerCase();
                if (!counter.hasOwnProperty(word)) {
                    counter[word] = 1;
                } else {
                    counter[word] = counter[word] + 1;
                }
            });
            return counter;
        }

        stream.on('data', function(data) {
            const chunk = data.toString();

            const fullData = lastWordInPrevChunk + chunk;

            const wordsList = fullData.match(/[A-Za-z]+/g);

            const lastWord = wordsList[wordsList.length - 1];
            const lastSymbolInChunk = chunk[chunk.length - 1];
            const isLastWordEndOfChunk = lastWord[lastWord.length - 1] === lastSymbolInChunk;
            lastWordInPrevChunk = isLastWordEndOfChunk ? lastWord : `${lastWord} `;

            const formattedWordList = wordsList.slice(0, wordsList.length - 1)

            storage.wordsCounter = countWords(formattedWordList, storage.wordsCounter);
        });

        stream.on('close', function() {
            const finishReadingTime = new Date();

            // Count last word when stream closed
            storage.wordsCounter = countWords([lastWordInPrevChunk], storage.wordsCounter);

            const sortedProps = sortProperties(storage.wordsCounter);
            const top10 = sortedProps.map((item) => ({ word: item[0], count: item[1] }));

            const finishTime = new Date();
            console.log('Top 10 words', top10);
            console.log('time for reading: ', getSeconds(finishReadingTime - startTime), 's');
            console.log('Time for top10: ', getSeconds(finishTime - startTime), 's');
            console.log('Total time: ', getSeconds(finishTime - startTime), 's');
        })
    }

    return {
        readFile,
    }
})();


readingModule.readFile('plain.txt');
