CHALLENGE 3

Result of module execution with file plain.txt (size ~360MB)

![img_1.png](img/plain.txt.png)


Reading by fs.readFile:

![img.png](img/img.png)

Reading by fs.createReadStream:

![img.png](img/read_stream.png)