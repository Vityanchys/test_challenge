const readingModule = (function () {
    const fs = require('fs');

    function getSeconds(time) {
        return new Date(time).getTime() / 1000;
    }

    function countWords(words) {
        const wordsCounter = {};

        words.forEach(function (wordItem) {
            const word = wordItem.toLowerCase();
            if (!wordsCounter.hasOwnProperty(word)) {
                wordsCounter[word] = 1;
            } else {
                wordsCounter[word] = wordsCounter[word] + 1;
            }
        });

        return wordsCounter;
    }

    function sortProperties(obj) {
        let sortable = [];

        for (let item in obj) {
            sortable.push([item, obj[item]]);
        }

        const sortedValues = sortable.sort(function (a, b) {
            return a[1] - b[1];
        });

        return sortedValues.reverse().splice(0, 5);
    }


    const getTop10 = function (data) {
        const wordsList = data.match(/[A-Za-z]+/g);

        const countedWordsObj = countWords(wordsList);
        const sortedArray = sortProperties(countedWordsObj);

        return sortedArray.map((item) => ({ word: item[0], count: item[1] }))
    }

    function readFile(fileName) {
        const startTime = new Date();

        const startReadingTime = new Date();
        fs.readFile(fileName, 'utf-8', (err, data) => {
            const finishReadingTime = new Date();
            if (err) throw err;

            const startTimeTop10 = new Date();
            const top10 = getTop10(data);

            const finishTime = new Date();

            console.log('Top 10 words', top10);
            console.log('time for reading: ', getSeconds(finishReadingTime - startReadingTime), 's');
            console.log('Time for top10: ', getSeconds(finishTime - startTimeTop10), 's');
            console.log('Total time: ', getSeconds(finishTime - startTime), 's');
        });
    }

    return {
        readFile,
    }
})();


readingModule.readFile('plain.txt');
