const nestedObj = {
    data: {
        info: {
            stuff: {
                thing: {
                    moreStuff: {
                        magicNumber: 44,
                        something: 'foo2',
                    }
                }
            },
            lol: 123
        }
    }
};


function contains(obj, value) {
    const arrayOfValues = Object.values(obj);
    const isValueExist = arrayOfValues.find((item) => item === value);
    if (!!isValueExist) {
        return true;
    }

    let key;

    for (key in obj) {
        if(obj.hasOwnProperty(key) && typeof obj[key] === "object") {
            return !!contains(obj[key], value);
        }
    }
}


const hastl = contains(nestedObj, 44);
const doesntHaveIt = contains(nestedObj, 'foo');

console.log(hastl)
console.log(doesntHaveIt)
